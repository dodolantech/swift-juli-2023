import React, { useState } from "react";
import {
    Typography,
    Card,
    Input,
    Switch,
    Option,
    Button,
    Textarea,
    Tooltip
}
    from "@material-tailwind/react";
import { InformationCircleIcon } from "@heroicons/react/24/outline";
import { Petunjuk } from "@/component/tooltip";
import { Form, DatePicker } from "antd"
import dayjs from "dayjs"


export function DataAcara() {
    const [form] = Form.useForm()
    let Acara2 = Form.useWatch("tampilkan_acara_2", form)

    const onFinish = (values) => {
        if (values.tanggal_acara_1) {
            values.tanggal_acara_1 = dayjs(values.tanggal_acara_1).format("YYYY-MM-DD")
        }
        if (values.tanggal_acara_2) {
            values.tanggal_acara_2 = dayjs(values.tanggal_acara_2).format("YYYY-MM-DD")
        }
        console.log('Success:', { acara: values });
    };
    console.log(Acara2)

    return (
        <Card shadow={false} className="p-5 mt-12">
            <Typography variant="h4" color="blue-gray">
                Data Acara
            </Typography>

            <Form className="mt-8 mb-2" onFinish={onFinish}
                form={form}
                initialValues={{
                    judul: "Halo",
                    sub_judul: "Tuhan",
                    nama_wanita_dahulu: true,
                    tampilkan_foto_profil: false
                }}
            >
                <div className="mb-2 grid grid-rows gap-6">

                    <Form.Item name="judul" rules={[{ required: true, message: 'Tentukan Judul' }]}>
                        <Input size="lg" label="Judul" required />
                    </Form.Item>
                    <Form.Item name="sub_judul" rules={[{ required: true, message: 'Tentukan Sub Judul' }]}>
                        <Textarea size="lg" label="Sub Judul" required />
                    </Form.Item>
                    <Form.Item valuePropName="checked" name="tampilkan_acara_2">
                        <Switch id="pakai" label="Tampilkan Acara 2"
                        />
                    </Form.Item>
                    <div className="md:grid md:grid-cols-2 md:gap-6">
                        <div className="mb-2 grid grid-rows gap-6">
                            <Form.Item name="nama_acara_1" rules={[{ required: true, message: 'Tentukan Nama Acara 1' }]}>
                                <Input size="lg" label=" Nama Acara 1" />
                            </Form.Item>
                            <Form.Item name="tanggal_acara_1" rules={[{ required: true, message: 'Tentukan Tanggal Acara 1' }]}
                                css={{
                                    ".ant-picker-input input::placeholder": {
                                        color: "grey"
                                    },
                                    ".ant-picker": {
                                        padding: 12,
                                        width: "100%",
                                        borderColor: "rgb(176, 190, 197)"
                                    }
                                }}
                            >
                                <DatePicker placeholder="Tanggal Acara 1" format={"YYYY-MM-DD"} />
                            </Form.Item>
                            <Form.Item name="jam_acara_1" rules={[{ required: true, message: 'Tentukan Jam Acara 1' }]}>
                                <Input size="lg" label="Jam Acara 1" />
                            </Form.Item>
                            <Form.Item name="tempat_acara_1" rules={[{ required: true, message: 'Tentukan Tempat Acara 1' }]}>
                                <Input size="lg" label="Tempat Acara 1" />
                            </Form.Item>
                            <Form.Item name="alamat_acara_1" rules={[{ required: true, message: 'Tentukan Alamat Acara 1' }]}>
                                <Input size="lg" label="Alamat Acara 1" />
                            </Form.Item>
                            <Form.Item name="maps_acara_1">
                                <Input size="lg" label="Link Google Maps Acara 1" />
                            </Form.Item>
                        </div>
                        {Acara2 && <div className="mb-2 mt-6 md:mt-0 grid grid-rows gap-6">
                            <Form.Item name="nama_acara_2" rules={[{ required: true, message: 'Tentukan Nama Acara 2' }]}>
                                <Input size="lg" label=" Nama Acara 2" />
                            </Form.Item>
                            <Form.Item name="tanggal_acara_2" rules={[{ required: true, message: 'Tentukan Tanggal Acara 2' }]}
                                css={{
                                    ".ant-picker-input input::placeholder": {
                                        color: "grey"
                                    },
                                    ".ant-picker": {
                                        padding: 12,
                                        width: "100%",
                                        borderColor: "rgb(176, 190, 197)"
                                    }
                                }}
                            >
                                <DatePicker placeholder="Tanggal Acara 2" format={"YYYY-MM-DD"} />
                            </Form.Item>
                            <Form.Item name="jam_acara_2" rules={[{ required: true, message: 'Tentukan Jam Acara 2' }]}>
                                <Input size="lg" label="Jam Acara 2" />
                            </Form.Item>
                            <Form.Item name="tempat_acara_2" rules={[{ required: true, message: 'Tentukan Tempat Acara 2' }]}>
                                <Input size="lg" label="Tempat Acara 2" />
                            </Form.Item>
                            <Form.Item name="alamat_acara_2" rules={[{ required: true, message: 'Tentukan Alamat Acara 2' }]}>
                                <Input size="lg" label="Alamat Acara 2" />
                            </Form.Item>
                            <Form.Item name="maps_acara_2">
                                <Input size="lg" label="Link Google Maps Acara 2" />
                            </Form.Item>
                        </div>}
                    </div>

                    <Form.Item name="judul_counting" rules={[{ required: true, message: 'Tentukan Judul Counting Days' }]}>
                        <Input size="lg" label="Judul Counting Days" required />
                    </Form.Item>
                    <Form.Item name="sub_judul_counting" rules={[{ required: true, message: 'Tentukan Sub Judul Counting Days' }]}>
                        <Textarea size="lg" label="Sub JudulCounting Days" required />
                    </Form.Item>
                    <Form.Item name="tanggal_counting" rules={[{ required: true, message: 'Tentukan Tanggal Counting Days' }]}
                        css={{
                            ".ant-picker-input input::placeholder": {
                                color: "grey"
                            },
                            ".ant-picker": {
                                padding: 12,
                                width: "100%",
                                borderColor: "rgb(176, 190, 197)"
                            }
                        }}
                    >
                        <DatePicker placeholder="Tanggal Counting Days" format={"YYYY-MM-DD"} />

                    </Form.Item>
                    <Switch id="samakan_2" label="Samakan Dengan Tanggal Acara 2"
                    />
                </div>

                <Form.Item>

                    <Button className="mt-6" fullWidth type="submit">
                        Simpan
                    </Button>


                </Form.Item>
            </Form>
        </Card>
    );
}

export default DataAcara;
