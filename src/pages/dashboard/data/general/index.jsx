import Uploader from "@/components/Uploader";
import useFetch from "@/hooks/useFetch";
import useFetchMutation from "@/hooks/useFetch/mutation";
import {
  Button,
  Card,
  Input,
  Option,
  Select,
  Typography,
} from "@material-tailwind/react";
import { Form } from "antd";
import { useEffect } from "react";
import { useState } from "react";

export function DataGeneral() {
  const [form] = Form.useForm();
  const { data, loading } = useFetch("/api/music");
  const [doUpload] = useFetchMutation("/api/storage/upload");
  const [doUnlink] = useFetchMutation("/api/storage/unlink");

  const [uploadedFiles, setUploadedFiles] = useState({});

  const dataFromApi = {
    judul: "Halo",
    sub_judul: "Tuhan",
    nama_wanita_dahulu: true,
    tampilkan_foto_profil: false,
    pengantinWanita:
      "https://api-swift.invitt.us/uploads/PW-IMG-2023-07-03-01-28-31-8899ab14780236aad61f685189f967dc.png",
  };

  const onFinish = async (values) => {
    // Prosesupload
    const files = await Promise.all(
      Object.entries(uploadedFiles).map(async ([key, value]) => {
        const formData = new FormData();
        formData.append("file", value);
        const result = await doUpload({ variables: formData });

        return {
          key,
          url: result.data?.url,
        };
      })
    );
    // end-Prosesupload

    const objFile = files.reduce(
      (prev, curr) => ({ ...prev, [curr.key]: curr.url }),
      {}
    );
    const payload = {
      general: {
        ...objFile,
        ...values,
      },
    };
    console.log("Success:", payload);

    // Delete data lama
    for (const file of files) {
      const urlLama = dataFromApi[file.key];
      if (urlLama && urlLama !== file.url) {
        // Hapus
        doUnlink({
          variables: { url: urlLama },
        });
      }
    }
  };

  return (
    <Card shadow={false} className="mt-12 p-5">
      <Typography variant="h4" color="blue-gray">
        Data General
      </Typography>

      <Form
        className="mt-8 mb-2"
        onFinish={onFinish}
        form={form}
        initialValues={dataFromApi}
      >
        <Form.Item
          name="pengantinWanita"
          rules={[{ required: true, message: "Pilih Foto Pengantin" }]}
        >
          <Uploader
            setUploadedFiles={setUploadedFiles}
            name="pengantinWanita"
            label="Upload Foto Pengantin Wanita"
          />
        </Form.Item>

        <div className="grid-rows mb-2 grid gap-6">
          <Form.Item
            name="template"
            rules={[{ required: true, message: "Pilih Template!" }]}
          >
            <Select label="Pilih Template">
              <Option value="Swift Lily">Swift Lily</Option>
              <Option value="Swift Adelia">Swift Adelia</Option>
            </Select>
          </Form.Item>

          <Form.Item
            name="bahasa"
            rules={[{ required: true, message: "Pilih Bahasa!" }]}
          >
            <Select label="Pilih Bahasa">
              <Option value="Bahasa Indonesia">Bahasa Indonesia</Option>
              <Option value="Bahasa Inggris">Bahasa Inggris</Option>
            </Select>
          </Form.Item>

          <Form.Item
            name="judul_acara"
            rules={[{ required: true, message: "Tentukan Judul Acara" }]}
          >
            <Input size="lg" label="Judul Acara" />
          </Form.Item>

          <Form.Item
            name="music"
            rules={[{ required: true, message: "Pilih lagu!" }]}
          >
            <Select label="Pilih Lagu" required>
              <Option value="Lagu 1">Lagu 1</Option>
              <Option value="Lagu 2">Lagu2</Option>
            </Select>
          </Form.Item>
        </div>

        <Form.Item>
          <Button className="mt-6" fullWidth type="submit">
            Simpan
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
}

export default DataGeneral;
