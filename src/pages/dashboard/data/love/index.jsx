import React, { useState } from "react";
import {
    Typography,
    Card,
    Input,
    Switch,
    Option,
    Button,
    Textarea,
    Tooltip
}
    from "@material-tailwind/react";
import { InformationCircleIcon } from "@heroicons/react/24/outline";
import { Petunjuk } from "@/component/tooltip";
import { Form, Button as Buttonantd } from "antd"


export function DataLive() {
    const [form] = Form.useForm()

    const onFinish = (values) => {
        console.log('Success:', { love: values });
    };

    return (
        <Card shadow={false} className="p-5 mt-12">
            <Typography variant="h4" color="blue-gray">
                Data Live Streaming
            </Typography>

            <Form className="mt-8 mb-2" onFinish={onFinish} form={form}
                initialValues={{
                    judul: "Halo",
                    sub_judul: "Tuhan",
                    nama_wanita_dahulu: true,
                    tampilkan_foto_profil: false
                }}
            >
                <div className="mb-2 grid grid-rows gap-6">
                    <Form.Item valuePropName="checked" name="pakai">
                        <Switch id="pakai" label="Menggunakan Fitur Love Story"
                        />
                    </Form.Item>
                    <Form.Item name="judul" rules={[{ required: true, message: 'Tentukan Judul' }]}>
                        <Input size="lg" label="Judul" />
                    </Form.Item>
                  
                    <Form.Item name="text1" rules={[{ required: true, message: 'Tentukan Love Story Part 1' }]}>
                        <Textarea size="lg" label="Love Story Part 1" />
                    </Form.Item>
                    <Form.Item name="text2" rules={[{ required: true, message: 'Tentukan Love Story Part 2' }]}>
                        <Textarea size="lg" label="Love Story Part 2" />
                    </Form.Item>
                    <Form.Item name="text3" rules={[{ required: true, message: 'Tentukan Love Story Part 3' }]}>
                        <Textarea size="lg" label="Love Story Part 3" />
                    </Form.Item>
                </div>
                <Form.Item>

                    <Button className="mt-6" fullWidth type="submit">
                        Simpan
                    </Button>


                </Form.Item>
            </Form>
        </Card>
    );
}

export default DataLive;
