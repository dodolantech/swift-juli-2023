import React, { useState } from "react";
import {
    Typography,
    Card,
    Input,
    Switch,
    Button,
    Textarea,
    Tooltip
}
    from "@material-tailwind/react";
import { InformationCircleIcon } from "@heroicons/react/24/outline";
import { Petunjuk } from "@/component/tooltip";
import { Form, Button as Buttonantd } from "antd"


export function DataMempelai() {
    const [form] = Form.useForm()

    const onFinish = (values) => {
        console.log('Success:', { mempelai: values });
    };

    return (
        <Card shadow={false} className="p-5 mt-12">
            <Typography variant="h4" color="blue-gray">
                Data Mempelai
            </Typography>

            <Form className="mt-8 mb-2" onFinish={onFinish} form={form}
                initialValues={{
                    judul: "Halo",
                    sub_judul: "Tuhan",
                    nama_wanita_dahulu: true,
                    tampilkan_foto_profil: false
                }}
            >

                <div className="mb-2">

                    <Form.Item name="judul" >
                        <Input size="lg" label="Judul" required />
                    </Form.Item>
                </div>
                <div className="mb-2">
                    <Form.Item name="sub_judul" >
                        <Textarea size="lg" label="Sub Judul" required />
                    </Form.Item>
                </div>


                <div className="md:flex flex-row w-full flex-wrap">
                    <div className="mb-2 basis-1/2">
                        <Form.Item valuePropName="checked" name="nama_wanita_dahulu">
                            <Switch id="nama_wanita_dahulu" label="Nama Wanita Dahulu"
                            />
                        </Form.Item>
                    </div>
                    <div className="mb-2 basis-1/2" >
                        <Form.Item name="tampilkan_foto_profil" valuePropName="checked">
                            <Switch id="tampilkan_foto_profil" label="Tampilkan Foto Profil" />
                        </Form.Item>
                    </div>
                </div>
                <div className="grid grid grid-cols-1 divide-y md:grid-cols-2 md:divide-x sm:divide-y-0">
                    <div className={`pb-2 md:pb-0 md:pr-4 grid grid-rows gap-4`}>
                        <Form.Item name="nama_lengkap_pria" >
                            <Input size="lg" label="Nama Lengkap Pria" required />
                        </Form.Item>
                        <Form.Item name="nama_panggilan_pria">
                            <Input size="lg" label="Nama Panggilan Pria" required />
                        </Form.Item>

                        <div className="relative">
                            <Petunjuk content={<><i >Contoh</i><br /><br />Putra dari:<br />Bapak<br />& Ibu</>} />
                            <Form.Item name="nama_orangtua_pria">
                                <Textarea label="Nama Orangtua Pria" />
                            </Form.Item>
                        </div>
                        <div className="relative">
                            <Petunjuk content={<><i >Contoh</i><br /><br />possible.wedding</>} />
                            <Form.Item name="instagram_pria">
                                <Input size="lg" label="Instagram Pria" />
                            </Form.Item>
                        </div>
                    </div>
                    <div className="pt-2 md:pt-0 md:pl-4 grid grid-rows gap-4">
                        <Form.Item name="nama_lengkap_wanita" required>
                            <Input size="lg" label="Nama Lengkap Wanita" />
                        </Form.Item>
                        <Form.Item name="nama_panggilan_Wanita" required>
                            <Input size="lg" label="Nama Panggilan Wanita" />
                        </Form.Item>
                        <div className="relative">
                            <Petunjuk content={<><i >Contoh</i><br /><br />Putra dari:<br />Bapak<br />& Ibu</>} />
                            <Form.Item name="nama_orangtua_wanita">
                                <Textarea label="Nama Orangtua Wanita" />
                            </Form.Item>
                        </div>

                        <div className="relative">
                            <Petunjuk content={<><i >Contoh</i><br /><br />possible.wedding</>} />
                            <Form.Item>
                                <Input size="lg" label="Instagram Wanita" name="instagram_Wanita" />
                            </Form.Item>
                        </div>
                    </div>
                </div>
                <Form.Item>

                    <Button className="mt-6" fullWidth type="submit">
                        Simpan
                    </Button>


                </Form.Item>
            </Form>
        </Card>
    );
}

export default DataMempelai;
