import React, { useState } from "react";
import {
    Typography,
    Card,
    Input,
    Switch,
    Button,
    Textarea,
    Tooltip
}
    from "@material-tailwind/react";
import { InformationCircleIcon } from "@heroicons/react/24/outline";
import { Petunjuk } from "@/component/tooltip";
import { Form, Button as Buttonantd } from "antd"


export function DataAyat() {
    const [form] = Form.useForm()

    const onFinish = (values) => {
        console.log('Success:', { ayat: values });
    };

    return (
        <Card shadow={false} className="p-5 mt-12">
            <Typography variant="h4" color="blue-gray">
                Data Ayat
            </Typography>

            <Form className="mt-8 mb-2" onFinish={onFinish} form={form}
                initialValues={{
                    judul: "Halo",
                    sub_judul: "Tuhan",
                    nama_wanita_dahulu: true,
                    tampilkan_foto_profil: false
                }}
            >
                <div className="mb-2">
                    <Form.Item name="judul" >
                        <Input size="lg" label="Judul" required />
                    </Form.Item>
                </div>
                <div className="mb-2">
                    <Form.Item name="isi" >
                        <Textarea size="lg" label="Isi Ayat" required />
                    </Form.Item>
                </div>
                <div className="mb-2">
                    <Form.Item name="sumber_kutipan" >
                        <Input size="lg" label="Sumber Kutipan" required />
                    </Form.Item>
                </div>

                <Form.Item>

                    <Button className="mt-6" fullWidth type="submit">
                        Simpan
                    </Button>


                </Form.Item>
            </Form>
        </Card>
    );
}

export default DataAyat;
