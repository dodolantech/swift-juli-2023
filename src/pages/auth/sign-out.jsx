import Loader from "@/components/Loader";
import useFetchMutation from "@/hooks/useFetch/mutation";
import { useEffect } from "react";

const SignOut = () => {
  const [doLogout] = useFetchMutation("/api/auth/logout", {
    onCompleted: () => {
      window.location.replace("/auth/sign-in");
    },
  });

  useEffect(() => {
    console.log("ll");
    doLogout({ variables: {} });
  }, []);

  return <Loader />;
};

export default SignOut;
