import useFetchMutation from "@/hooks/useFetch/mutation";
import {
  Alert,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Checkbox,
  Input,
  Typography,
} from "@material-tailwind/react";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export function SignIn() {
  const [error, setError] = useState();
  const [doMutation, { loading }] = useFetchMutation("/api/auth/login", {
    onCompleted: () => {
      window.location.replace("/");
    },
    onError: (err) => setError(err),
  });

  useEffect(() => {
    if (error) {
      setTimeout(() => {
        setError(undefined);
      }, 3000);
    }
  }, [error]);

  const handleSubmit = (e) => {
    e.stopPropagation();
    e.preventDefault();
    const formData = new FormData(e.currentTarget);

    const slug = formData.get("slug");
    const password = formData.get("password");
    const loginpayload = { slug, password };
    doMutation({ variables: loginpayload });
  };
  return (
    <>
      <img
        src="https://images.unsplash.com/photo-1497294815431-9365093b7331?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1950&q=80"
        className="absolute inset-0 z-0 h-full w-full object-cover"
      />
      <div className="absolute inset-0 z-0 h-full w-full bg-black/50" />
      <div className="container mx-auto p-4">
        <form onSubmit={handleSubmit}>
          <Card className="absolute top-2/4 left-2/4 w-full max-w-[24rem] -translate-y-2/4 -translate-x-2/4">
            <CardHeader
              variant="gradient"
              color="blue"
              className="mb-4 grid h-28 place-items-center"
            >
              <Typography variant="h3" color="white">
                Sign In
              </Typography>
            </CardHeader>
            <CardBody className="flex flex-col gap-4">
              {error && (
                <Alert color="red" variant="gradient">
                  <span>{error.message}</span>
                </Alert>
              )}
              <Input
                required
                disabled={loading}
                name="slug"
                label="Username"
                size="lg"
              />
              <Input
                required
                disabled={loading}
                type="password"
                name="password"
                label="Password"
                size="lg"
              />
              <div className="-ml-2.5">
                <Checkbox label="Remember Me" />
              </div>
            </CardBody>
            <CardFooter className="pt-0">
              <Button
                disabled={loading}
                type="submit"
                variant="gradient"
                fullWidth
              >
                {loading ? "Signing In..." : "Sign In"}
              </Button>
              <Typography variant="small" className="mt-6 flex justify-center">
                Don't have an account?
                <Link to="/auth/sign-up">
                  <Typography
                    as="span"
                    variant="small"
                    color="blue"
                    className="ml-1 font-bold"
                  >
                    Sign up
                  </Typography>
                </Link>
              </Typography>
            </CardFooter>
          </Card>
        </form>
      </div>
    </>
  );
}

export default SignIn;
