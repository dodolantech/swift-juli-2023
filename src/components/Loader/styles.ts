import { css } from "@emotion/react";

export const cssLoaderStyle = (blockScreen) =>
  css({
    display: "flex",
    ...(blockScreen
      ? {
          position: "fixed",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
        }
      : {
          height: 300,
          margin: "20px 0",
        }),
    zIndex: 9999,

    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(255,255,255,.5)",
    svg: {
      width: 100,
    },
  });
