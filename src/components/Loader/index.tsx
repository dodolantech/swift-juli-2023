import { cssLoaderStyle } from "./styles";

const PRIMARY_COLOR = "#54A1E9";

const Loader = ({ blockScreen = true, size }: LoaderProps) => (
  <div css={cssLoaderStyle(blockScreen)}>
    <svg
      {...(size && {
        css: {
          width: size,
          height: size,
        },
      })}
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      style={{ display: "block" }}
      viewBox="0 0 100 100"
      preserveAspectRatio="xMidYMid"
    >
      <circle
        cx={50}
        cy={50}
        r={0}
        fill="none"
        stroke={PRIMARY_COLOR}
        strokeWidth={4}
      >
        <animate
          attributeName="r"
          repeatCount="indefinite"
          dur="1s"
          values="0;43"
          keyTimes="0;1"
          keySplines="0 0.2 0.8 1"
          calcMode="spline"
          begin="0s"
        />
        <animate
          attributeName="opacity"
          repeatCount="indefinite"
          dur="1s"
          values="1;0"
          keyTimes="0;1"
          keySplines="0.2 0 0.8 1"
          calcMode="spline"
          begin="0s"
        />
      </circle>
      <circle
        cx={50}
        cy={50}
        r={0}
        fill="none"
        stroke="#3c4d4f"
        strokeWidth={4}
      >
        <animate
          attributeName="r"
          repeatCount="indefinite"
          dur="1s"
          values="0;43"
          keyTimes="0;1"
          keySplines="0 0.2 0.8 1"
          calcMode="spline"
          begin="-0.5s"
        />
        <animate
          attributeName="opacity"
          repeatCount="indefinite"
          dur="1s"
          values="1;0"
          keyTimes="0;1"
          keySplines="0.2 0 0.8 1"
          calcMode="spline"
          begin="-0.5s"
        />
      </circle>
    </svg>
  </div>
);

interface LoaderProps {
  blockScreen?: boolean;
  size?: number;
}

export default Loader;
