import { resizeImage } from "@/helpers/images";
import { Alert } from "@material-tailwind/react";
import { Button, Typography, Upload } from "antd";
import { useEffect } from "react";
import { useState } from "react";

const Uploader = ({
  name,
  setUploadedFiles,
  label,
  maxCount,
  onUpload,
  onRemove,
  value,
  onChange,
}) => {
  const [imgUrl, setImgUrl] = useState(value || "");
  const [error, setError] = useState("");

  useEffect(() => {
    if (error) {
      setTimeout(() => {
        setError("");
      }, 3000);
    }
  }, [error]);

  return (
    <div css={{ marginBottom: 32 }}>
      <Typography.Paragraph strong>{label}</Typography.Paragraph>
      {error && (
        <Alert css={{ marginBottom: 10 }} color="red" variant="gradient">
          <span>{error}</span>
        </Alert>
      )}
      <Upload
        fileList={[]}
        maxCount={maxCount}
        customRequest={async ({ file, onSuccess }) => {
          const finalFile = await resizeImage({ file, maxSize: 1800 });
          const sizeInM = finalFile.size / 1000_000;
          if (sizeInM > 1) {
            setError("Max file size is 1MB");
            return;
          }
          const url = URL.createObjectURL(finalFile);
          setImgUrl(url);
          onChange?.(url);
          setUploadedFiles((prev) => {
            prev[name] = file;
            return prev;
          });
          onSuccess?.();
          onUpload?.(file);
        }}
        accept="image/jpeg,image/png"
      >
        {imgUrl ? (
          <>
            <Button
              onClick={(e) => {
                e.stopPropagation();
                setImgUrl("");
                onChange?.(undefined);
                onRemove?.();
                setUploadedFiles((prev) => {
                  delete prev[name];
                  return prev;
                });
              }}
              type="link"
              danger
            >
              Remove
            </Button>
            <Button type="link">Update</Button>
            <img css={{ width: 400 }} src={imgUrl} />
          </>
        ) : (
          <Button> +Upload</Button>
        )}
      </Upload>
    </div>
  );
};

export default Uploader;
