import Loader from "@/components/Loader";
import useFetch from "@/hooks/useFetch";
import { Navigate, useLocation } from "react-router-dom";
import AuthContext from ".";

const PUBLIC_PATH = /(\/auth\/sign-in|\/auth\/sign-up)/i;

const AuthProvider = ({ children }) => {
  const { data, loading } = useFetch(`/api/auth/me`);
  const { pathname } = useLocation();
  console.log(pathname);
  if (loading) {
    return <Loader />;
  }
  const isLoggedin = data?.id;
  const publicPath = PUBLIC_PATH.test(pathname);
  const protectedPath = !publicPath;
  const nonLoginInprotectedPath = protectedPath && !isLoggedin;
  const loggedinInPublicPath = publicPath && isLoggedin;

  if (nonLoginInprotectedPath) {
    return <Navigate to="/auth/sign-in" />;
  }
  if (loggedinInPublicPath) {
    return <Navigate to="/" />;
  }

  return <AuthContext.Provider value={{}}>{children}</AuthContext.Provider>;
};

export default AuthProvider;
