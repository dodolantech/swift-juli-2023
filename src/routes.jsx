import { SignIn } from "@/pages/auth";
import {
  DataAcara,
  DataAyat,
  DataGeneral,
  DataLive,
  DataMempelai,
  Home,
  Notifications,
  Profile,
  Tables,
} from "@/pages/dashboard";
import {
  ArrowRightOnRectangleIcon,
  BellIcon,
  HomeIcon,
  TableCellsIcon,
  UserCircleIcon,
} from "@heroicons/react/24/solid";
import SignOut from "./pages/auth/sign-out";

const icon = {
  className: "w-5 h-5 text-inherit",
};

export const routes = [
  {
    layout: "dashboard",
    pages: [
      {
        icon: <HomeIcon {...icon} />,
        name: "dashboard",
        path: "/home",
        element: <Home />,
      },
      {
        icon: <HomeIcon {...icon} />,
        name: "Data Mempelai",
        exclude: true,
        path: "/data/mempelai",
        element: <DataMempelai />,
      },
      {
        icon: <HomeIcon {...icon} />,
        name: "Data Ayat",
        exclude: true,
        path: "/data/ayat",
        element: <DataAyat />,
      },
      {
        icon: <HomeIcon {...icon} />,
        name: "Data General",
        exclude: true,
        path: "/data/general",
        element: <DataGeneral />,
      },
      {
        icon: <HomeIcon {...icon} />,
        name: "Data Live Streaming",
        exclude: true,
        path: "/data/live",
        element: <DataLive />,
      },
      {
        icon: <HomeIcon {...icon} />,
        name: "Data Acara",
        exclude: true,
        path: "/data/acara",
        element: <DataAcara />,
      },
      {
        icon: <UserCircleIcon {...icon} />,
        name: "profile",
        path: "/profile",
        element: <Profile />,
      },
      {
        icon: <TableCellsIcon {...icon} />,
        name: "tables",
        path: "/tables",
        element: <Tables />,
      },
      {
        icon: <BellIcon {...icon} />,
        name: "notifactions",
        path: "/notifactions",
        element: <Notifications />,
      },
    ],
  },
  {
    title: "auth pages",
    layout: "auth",
    pages: [
      {
        icon: <ArrowRightOnRectangleIcon {...icon} />,
        name: "sign in",
        path: "/sign-in",
        element: <SignIn />,
        type: "route",
      },
      {
        icon: <ArrowRightOnRectangleIcon {...icon} />,
        name: "Sign Out",
        path: "/sign-out",
        element: <SignOut />,
      },
    ],
  },
];

export default routes;
