import { useCallback, useEffect, useMemo, useRef } from "react";

import useSwr from "swr";

import { UseFetchOption } from "./interface";

const useFetch = <DT = Record<string, unknown>>(
  url: string,
  options?: UseFetchOption
) => {
  const onError = useMemo(() => options?.onError, [options]);
  const onCompleted = useMemo(() => options?.onCompleted, [options]);
  const revalidateOnFocus = useMemo(
    () => options?.revalidateOnFocus,
    [options]
  );
  const skip = useMemo(() => options?.skip, [options]);
  const memoizedVariable = useMemo<any>(
    () => options?.variables || {},
    [options]
  );
  const variables = useRef(memoizedVariable || {});

  const urlUsed = useMemo(() => {
    if (!variables) {
      return url;
    }

    const varKeys = Object.keys(memoizedVariable);
    let prefix = "";
    const searchParams = new URLSearchParams();
    varKeys.forEach((key) => {
      searchParams.append(key, memoizedVariable[key]);
      prefix = "?";
    });

    return `${url}${prefix}${searchParams.toString()}`;
  }, [url, memoizedVariable]);

  const fetcher = useCallback((fetcherUrl: string) => {
    const headers = new Headers();

    return fetch(fetcherUrl, {
      headers,
    }).then((res) => res.json());
  }, []);
  const usedUrl = useMemo(() => {
    if (skip) return null;
    return urlUsed;
  }, [skip, urlUsed]);

  const {
    data,
    error,
    isLoading,
    mutate: refetch,
  } = useSwr<DT>(usedUrl, fetcher, {
    onError: (err) => {
      if (typeof onError === "function") onError(err);
    },
    onSuccess: onCompleted,
    revalidateOnFocus,
  });

  useEffect(() => {
    if (memoizedVariable) variables.current = memoizedVariable;
  }, [memoizedVariable]);

  return {
    data,
    loading: isLoading,
    error,
    refetch,
  };
};

export default useFetch;
