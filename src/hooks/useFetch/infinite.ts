import { useCallback, useEffect, useMemo, useRef } from "react";

import useSwr, { SWRInfiniteKeyLoader } from "swr/infinite";

import { UseFetchOption } from "./interface";

const useFetch = <DT = Record<string, unknown>>(
  url: string,
  options: UseFetchOption & { getKey: SWRInfiniteKeyLoader }
) => {
  const onError = useMemo(() => options?.onError, [options]);
  const onCompleted = useMemo(() => options?.onCompleted, [options]);
  const revalidateOnFocus = useMemo(
    () => options?.revalidateOnFocus,
    [options]
  );

  const memoizedVariable = useMemo<any>(
    () => options?.variables || {},
    [options]
  );
  const variables = useRef(memoizedVariable || {});

  const fetcher = useCallback((fetcherUrl: string) => {
    const headers = new Headers();

    return fetch(fetcherUrl, {
      headers,
    }).then((res) => res.json());
  }, []);

  const getKey = useMemo(() => options.getKey, [options]);

  const {
    data,
    error,
    isLoading,
    mutate: refetch,
    setSize,
    size,
  } = useSwr<DT>(getKey, fetcher, {
    onError: (err) => {
      if (typeof onError === "function") onError(err);
    },
    onSuccess: onCompleted,
    revalidateOnFocus,
  });

  useEffect(() => {
    if (memoizedVariable) variables.current = memoizedVariable;
  }, [memoizedVariable]);

  return {
    data,
    loading: isLoading,
    error,
    refetch,
    setSize,
    size,
  };
};

export default useFetch;
