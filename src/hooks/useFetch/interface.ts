export type UnknonwRecord = Record<string, unknown>;

export type VariablesType = Record<string, any>;

export interface UseFetchOption {
  variables?: VariablesType;
  onCompleted?: (data: any) => void;
  revalidateOnFocus?: boolean;
  onError?: (error: FetchError) => void;
  beforeSend?: (payload: any) => any;
  onFinal?: () => void;
  notifyLoading?: boolean;
  skip?: boolean;
  behavior?: 'append';
}

export type DefaultRecord = Record<string, unknown>;

export interface FetchError extends Error {
  error?: any;
}

export interface DoMutationType<DT> extends RequestInit {
  variables: DT | FormData;
}

export type MutationFuncType<DT, TBody> = (variables?: DoMutationType<TBody>) => Promise<DT>;

export interface MutaitonReturn<DT> {
  loading: boolean;
  data?: DT;
  error?: FetchError;
}
