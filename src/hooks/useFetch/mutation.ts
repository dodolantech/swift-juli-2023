import { useCallback, useMemo, useState } from 'react';

import type { FetchError, UnknonwRecord, UseFetchOption } from './interface';

interface DoMutationType<DT> extends RequestInit {
  variables: DT | FormData;
  url?: string;
}

type MutationFuncType<DT, TBody> = (variables?: DoMutationType<TBody>) => Promise<DT>;

interface MutaitonReturn<DT> {
  loading: boolean;
  data: DT | undefined;
  error?: FetchError;
}

const useFetchMutation = <DT, TBody = UnknonwRecord>(
  url: string,
  options?: Omit<UseFetchOption, 'variables'>,
): [MutationFuncType<DT, TBody>, MutaitonReturn<DT>] => {
  const { onCompleted: _0, onError: _1, beforeSend: _2, onFinal: _3 } = options || {};

  const onFinal = useMemo(() => options?.onFinal, [options]);
  const onError = useMemo(() => options?.onError, [options]);
  const onCompleted = useMemo(() => options?.onCompleted, [options]);
  const beforeSend = useMemo(() => options?.beforeSend, [options]);
  const [data, setData] = useState<DT | undefined>(undefined);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<FetchError | undefined>(undefined);

  // @ts-ignore
  const doMutation: MutationFuncType<DT, TBody> = useCallback(
    async (option: DoMutationType<TBody>) => {
      const { variables: newVariables, url: newUrl, method = 'post' } = option;
      setLoading(true);

      try {
        let dataToSend = newVariables;
        if (typeof beforeSend === 'function') {
          dataToSend = await beforeSend(dataToSend);
        }

        const headers = new Headers();
        if (!(dataToSend instanceof FormData)) {
          headers.append(
            'Content-Type',
            dataToSend instanceof FormData ? 'application/x-www-form-urlencoded' : 'application/json',
          );
        }
        const isBody = Boolean(Object.keys(newVariables || {}).length) || dataToSend instanceof FormData;

        const response = await fetch(newUrl || url, {
          method: method || 'POST',
          credentials: 'include',
          headers,
          ...(isBody && {
            body: dataToSend instanceof FormData ? dataToSend : JSON.stringify(dataToSend),
          }),
        });
        const { status } = response;
        const isOk = status >= 200 && status < 300;
        const responseData = await response.json();
        if (!isOk) throw responseData;

        if (typeof onCompleted === 'function') onCompleted(responseData);
        setData(responseData);
        return { data: responseData };
      } catch (error) {
        const realError = error as FetchError;
        if (typeof onError === 'function') {
          onError(realError);
        }
        setError(realError);
        return { error: realError };
      } finally {
        if (typeof onFinal === 'function') onFinal();
        setLoading(false);
      }
    },

    // eslint-disable-next-line react-hooks/exhaustive-deps
    [url],
  );

  return [
    doMutation,
    {
      data,
      loading,
      error,
    },
  ];
};

export default useFetchMutation;
