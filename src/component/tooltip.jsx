import {
    Tooltip,
    Typography
}
    from "@material-tailwind/react";
import { InformationCircleIcon } from "@heroicons/react/24/outline";

export const Petunjuk = (props) => {
    const { content } = props
    return (
        <Tooltip content={
            <div className="w-80">

                <Typography
                    variant="small"
                    color="white"
                    className="font-normal opacity-80"
                >
                    {content}
                </Typography>
            </div>
        }>
            <InformationCircleIcon
                strokeWidth={2}
                className="text-blue-gray-500 w-5 h-5 cursor-pointer"
                css={{
                    position: "absolute",
                    top: 10,
                    right: 10,
                    zIndex: 1
                }}

            />
        </Tooltip>
    )
}